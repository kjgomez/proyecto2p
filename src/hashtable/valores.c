#include <stdio.h>
#include <stdlib.h>
#include "hashtable.h"

void **valores(hashtable *tabla, int *conteo){
	void **valores=malloc((tabla->elementos)*sizeof(void *));
	int cont=0;
	int i;
	for(i=0;i<(tabla->numeroBuckets);i++){	
		objeto *obj=(tabla->buckets)[i];
		while(obj!=NULL){
			valores[cont]=obj->valor;
			obj=obj->siguiente;
			cont++;
		}
	}
	*conteo= cont;
	return valores;


}
