#include <stdio.h>
#include <stdlib.h>
#include "hashtable.h"


//Se debe crear una estructura hashtable y a cada elementos se lo inicializa como corresponga.
hashtable *crearHashTable(int numeroBuckets){
	hashtable *ht= (hashtable *) malloc(sizeof(hashtable));
	if(ht == NULL){
		printf("No existe espacio de memoria");
                return NULL;
        }
	ht->id=rand();
	ht->elementos=0; //Se asigna elementos, id y numero de buckets.
	ht->numeroBuckets=numeroBuckets;
	objeto **buckets= (objeto **) malloc (numeroBuckets*sizeof(objeto *)); //Se asigna espacio de memoria para los buckets de tipo objeto en la hashtable.
	if(buckets == NULL){
		printf("No existe espacio de memoria");
                return NULL;
        }
	ht->buckets= buckets; 
	for(int i=0;i<numeroBuckets;i++){
		(ht->buckets)[i]=NULL;		
	
	} //Se inicializa cada bucket de la hashtable en NULL.
	return ht;
}
