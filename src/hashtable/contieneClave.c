#include <stdio.h>
#include <stdlib.h>
#include "hashtable.h"

int contieneClave(hashtable *tabla, char *clave){
	int index= (long int)((hash((unsigned char *)clave))%(tabla->numeroBuckets));
	objeto *o=(tabla->buckets)[index];
	while(o!=NULL){
		if(strcmp(o->clave,clave)==0){
			return 1;
		}
		o=o->siguiente;
	}
	return 0;
}

