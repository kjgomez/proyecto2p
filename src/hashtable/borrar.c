#include <stdio.h>
#include <stdlib.h>
#include "hashtable.h"

void borrar(hashtable *tabla){
	int i;
	for(i=0;i<(tabla->numeroBuckets);i++){	//Se recorre la hashtable por cada bucket
		objeto *o= (tabla->buckets)[i];
		while(o!=NULL){ //Se recorren los elementos de cada bucket y se libera la memoria.
			objeto *temp=o;
			o=o->siguiente;
			free(temp);
		}
		(tabla->buckets)[i]=NULL; //Una vez se libere cada objeto, se dejan los buckets en NULL	
	}
	tabla->elementos=0; //Los elementos de la tabla deberían ser 0 una vez borrado todo.
}
