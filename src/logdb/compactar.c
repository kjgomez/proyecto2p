#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <fcntl.h>
#include "logdb.h"
#include "csapp.h"

void compactar(conexionlogdb *conexion){
	if (conexion==NULL){
		printf("Error con la conexión");
		return;
	}	
	char orden[BUFLEN]="compactar";
	rio_t rio;
	int env=rio_writen(conexion->sockdf,orden,BUFLEN);
	if(env<0){
		printf("Error en el envío");
		return;
	}	
	printf("Orden enviada");
	char buf[BUFLEN] =  { 0 };
	rio_readinitb(&rio,conexion->sockdf);
	int resp= rio_readlineb(&rio,buf,BUFLEN);
	if(resp<0){
		printf("Error al obtener respuesta");
		return;	
	}
}
