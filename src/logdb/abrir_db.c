#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <fcntl.h>
#include "logdb.h"
#include "csapp.h"


int abrir_db(conexionlogdb *conexion, char *nombre_db){
	if (conexion==NULL){
		return -1;
	}	
	char orden[BUFLEN];
	sprintf(orden,"abrir %s",nombre_db);
	rio_t rio;
	int env=rio_writen(conexion->sockdf,orden,BUFLEN);
	if(env<0){
		return -1;
	}	
	printf("Orden enviada");
	char buf[BUFLEN] =  { 0 };
	rio_readinitb(&rio,conexion->sockdf);
	int resp= rio_readlineb(&rio,buf,BUFLEN);
	if(resp<0){	
		printf("Error al recibir");
		return -1;	
	}
	return 0;
}
