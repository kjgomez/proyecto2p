#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <fcntl.h>
#include "logdb.h"
#include "csapp.h"


char *get_val(conexionlogdb *conexion, char *clave){
	char orden[BUFLEN];
	sprintf(orden,"get %s",clave);
	rio_t rio;
	int env=rio_writen(conexion->sockdf,orden,BUFLEN);
	if(env<0){
		return "Error al enviar";
	}	
	printf("Orden enviada");

	char *buf=malloc(sizeof(char *)*BUFLEN);
	rio_readinitb(&rio,conexion->sockdf);
	int resp= rio_readlineb(&rio,buf,BUFLEN);
	if(resp<0){
		return "Error al recibir";	
	}
	return buf;
	
}
