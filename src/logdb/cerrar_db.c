#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <fcntl.h>
#include "logdb.h"
#include "csapp.h"

void cerrar_db(conexionlogdb *conexion){	
	if (conexion==NULL){
		return;
	}	
	char orden[BUFLEN]="cerrar";
	int env=rio_writen(conexion->sockdf,orden,BUFLEN);
	if(env<0){
		return;
	}	
	printf("Orden enviada");
}
