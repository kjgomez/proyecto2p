#include <sys/types.h>          
#include <sys/stat.h>
#include <stdio.h>              
#include <stdlib.h>             
#include <stddef.h>             
#include <string.h>             
#include <unistd.h>             
#include <signal.h>              
#include <netdb.h> 
#include <errno.h> 
#include <syslog.h> 
#include <sys/socket.h> 
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/resource.h>
#include <ctype.h>
#include "logdb.h"

conexionlogdb *conectar_db(char *ip, int puerto){
	conexionlogdb *conect= malloc(sizeof(conexionlogdb *));
	memset(&conect, 0, sizeof(conect));
	conect->ip=ip;
	conect->puerto=puerto;
	conect->id_sesion=0;
	conect->nombredb="";
	
	struct sockaddr_in direccion_cliente;
	memset(&direccion_cliente, 0, sizeof(direccion_cliente));
	direccion_cliente.sin_family = AF_INET;		
	direccion_cliente.sin_port = htons(puerto);		
	direccion_cliente.sin_addr.s_addr = inet_addr(ip) ;


	int sockfd;
	int numsec, fd;

	for (numsec = 1; numsec <= MAXSLEEP; numsec <<= 1) { 
		if (( fd = socket(direccion_cliente.sin_family, SOCK_STREAM, 0)) < 0) 
			sockfd=-1; 

		if (connect(fd,(struct sockaddr *)&direccion_cliente,sizeof(direccion_cliente)) == 0) { /* * Conexión aceptada. */ 
			sockfd=fd;
		} 
		close(fd); 				//Si falla conexion cerramos y creamos nuevo socket
		if (numsec <= MAXSLEEP/2)
			sleep( numsec); 
	} 
	if(sockfd<0){
		printf("Fallo al conectar");
		return NULL;
	}
	conect->sockdf= sockfd;
	return conect;
}
