#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <fcntl.h>
#include "db.h"

int main(int argc, char *argv[]){
	if(argc!=4){
		printf("Numero incorrecto de argumentos\n");
		printf("Use: ./logdb <directorio> <ip> <puerto>");
		exit(-1);
	}
	char *directorio=argv[1];
	char *ip=argv[2];
	int puerto=atoi(argv[3]);
	
	conexionlogdb *conexion=conectar_db(ip,puerto);
	if(conexion==NULL){
		printf("No se pudo conectar\n");
		exit(-1);
	}

	bdabierta *baseabierta=malloc(sizeof(bdabierta));
	
	int clfd=-1;
	while(1){
		if(clfd == -1 ){
			clfd= accept(conexion->sockdf, NULL, NULL);
			
		}
		char buf[BUFLEN]={0};
		int n = recv(clfd,buf,BUFLEN,0);  
	    	if(n == 0){                     //cliente cerro conexion o dejo de existir
		    close(clfd);
		    clfd = -1;
		    continue;            
		}
		else{
			char orden[10]={0};
			char *arg= strchr(buf,' ');
			strncpy(orden,buf,strlen(buf)-strlen(arg));
			char resp[BUFLEN];
			if (strcmp(orden,"crear")==0)
			{
				int crear=crear_db(&arg[1],directorio);
				sprintf(resp,"Respuesta: %d",crear);
				send(clfd,resp, strlen(resp), 0);
			}
			else if(strcmp(orden,"abrir")==0)
			{
				int abrir=abrir_db(conexion,&arg[1],directorio,baseabierta);
				
				sprintf(resp,"Respuesta: %d",abrir);
				send(clfd,resp, strlen(resp), 0);
			}
			else if(strcmp(orden,"put")==0)
			{
				char clave[100]={0};
				char *valor=strrchr(buf,' ');
				strncpy(clave,arg,strlen(&arg[1])-strlen(&valor[1]));
				int put=put_val(&clave[1],&valor[1],baseabierta);
				sprintf(resp,"Respuesta: %d",put);
				send(clfd,resp, strlen(resp), 0);
			}
			else if(strcmp(orden,"get")==0)
			{
				char *get=get_val(&arg[1],baseabierta);
				sprintf(resp,"Respuesta: %s",get);
				send(clfd,resp, strlen(resp), 0);
			}
			else if(strcmp(orden,"eliminar")==0)
			{
				int elim=eliminar(&arg[1],baseabierta);
				sprintf(resp,"Respuesta: %d",elim);
				send(clfd,resp, strlen(resp), 0);
			}
			else if(strcmp(orden,"compactar")==0)
			{
				compactar(conexion,baseabierta);
				send(clfd,"Compactado",9,0);
			}
			else if(strcmp(orden,"cerrar")==0)
			{
				cerrar_db(conexion,baseabierta);
				send(clfd,"Cerrado", 8, 0);
			}
			else{
				printf("No se puedo hacer ninguna operación");

			}
	 	}
		
	}
	return 0;
}
	



