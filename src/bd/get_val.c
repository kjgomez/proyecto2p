#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "hashtable.h"
#include "csapp.h"
#include "db.h"

char *get_val(char *clave, bdabierta *baseabierta){
	int fd= baseabierta->fdlog;
	if((contieneClave(baseabierta->ht,clave))!=1){
		return NULL;	
	}	
	int dist= *(int *) get(baseabierta->ht,clave);
	lseek(fd,dist,SEEK_SET);
	char buf[200];
	rio_t rio;
	rio_readinitb(&rio,fd);
	rio_readlineb(&rio,buf,200);
	reemplazarCarater(buf,'\n',0);
	char *x=strchr(buf,':');
	char *valor=&x[1];
	if(strcmp(valor,"@T!")==0){
		return NULL;	
	}
	return valor;

}
