#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "db.h"


void reemplazarCarater(char *str, char viejo, char nuevo){
	while(*str != 0){
		if(*str == viejo){
			*str = nuevo;
			return;
		}
		str++;
	}
}
