#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "hashtable.h"
#include "csapp.h"
#include "db.h"

void cerrar_db(conexionlogdb *conexion,bdabierta *baseabierta){
	int fd= baseabierta->fdlog;
	int fd2= open(baseabierta->rutaindex,O_RDWR|O_CREAT|O_TRUNC,S_IRWXU);
	int n1;
	char **clavs= claves(baseabierta->ht,&n1);
	for (int i=0;i<n1;i++){
		char str[200];
		sprintf(str,"%s:%s\n",clavs[i],(char *)get(baseabierta->ht,clavs[i]));
		rio_writen(fd2,str,strlen(str));
	}
	close(fd);
	close(fd2);
	baseabierta->rutalog=NULL;
	baseabierta->rutaindex=NULL;
	borrar(baseabierta->ht);
	free(baseabierta->ht);
	baseabierta->fdlog=0;	
	conexion->ip=NULL;
	conexion->puerto=0;
	conexion->sockdf=0;
	conexion->id_sesion=0;
	conexion->nombredb=NULL;
}
