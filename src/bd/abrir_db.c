#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "csapp.h"
#include "db.h"
#define TAM 10000
#define LINE_SIZE 256

int abrir_db(conexionlogdb *conexion, char *nombre_db, char *directorio, bdabierta* baseabierta){
	int fd1,fd2;
	
	if (directorio==NULL){
		printf("La Base no ha sido creada");
		return 0;	
	}
	
	fd1= open(directorio,O_RDWR|O_CREAT);
	if(fd1<0){
		printf("Error de archivo");
		return 0;
	}
	
	char ind[200];
	sprintf(ind,"%sindex",directorio);
	fd2= open(ind,O_RDWR|O_CREAT|O_TRUNC);
	if(fd2<0){
		printf("Error de archivo");
		return 0;
	}
	hashtable *tabla;
	tabla= crearHashTable(TAM);
	llenarHash(tabla,ind);
	baseabierta->rutalog=directorio;
	baseabierta->rutaindex=ind;
	baseabierta->ht=tabla;
	baseabierta->fdlog=fd1;
	conexion->nombredb=nombre_db;
	return 1;
			
}


char* directorio(char *nombre_db){
	FILE *file = fopen("/confg", "r");
	char buf[LINE_SIZE] = {0};
	if(file != NULL){
		while(fgets(buf, LINE_SIZE, file) != NULL){
			reemplazarCarater(buf,'\n', 0);
			char *nombre = strtok(buf,":");
			char *directorios = strtok(NULL,":");		
			char *directorio=strtok(directorios," ");
			if(strcmp(nombre_db, nombre)==0){
				return directorio;		
			}
			memset(buf,0, LINE_SIZE);
		}
		
	}
	return NULL;
}

void llenarHash(hashtable *tabla, char *archivo){
	FILE *file = fopen(archivo, "r");
	char buf[LINE_SIZE] = {0};
	if(file != NULL){
		while(fgets(buf, LINE_SIZE, file) != NULL){
			reemplazarCarater(buf,'\n', 0);
			char *clave = strtok(buf,":");
			char *valor = strtok(NULL,":");		
			put(tabla, strdup(clave), strdup(valor));
			memset(buf,0, LINE_SIZE);
		}
	}

}


