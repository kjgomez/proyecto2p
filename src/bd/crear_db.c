#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "csapp.h"
#include "db.h"


int crear_db(char *nombre_db, char *directorio){
	int fd1,fd2;	
	char arch[200];
	sprintf(arch,"%s/%s",directorio,nombre_db);
	fd1= open(arch,O_RDONLY,0);
	if (fd1>0){
		printf("Ya existe la base de datos");
		return 0;
	}
	else{
		fd1= open(arch,O_RDWR|O_CREAT,S_IRWXU);
	}
	fd2= open("./confg",O_RDWR|O_CREAT|O_APPEND,S_IRWXU);
	if (fd2<0){
		printf("Error de archivo");
		return 0;
	}
	char str[200];
	sprintf(str,"%s:%s/%s %s/%sindex \n",nombre_db,directorio,nombre_db,directorio,nombre_db);
	rio_writen(fd2,str,strlen(str));
	close(fd1);
	close(fd2);
	return 1;
	 
}
