#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "csapp.h"
#include "db.h"


void compactar(conexionlogdb *conexion,bdabierta *baseabierta){
	int *conteo=0;	
	char **keys=claves(baseabierta->ht,conteo);
	int fd1;
	char arch[200];
	sprintf(arch,"%s%s",baseabierta->rutalog,"compacto");
	fd1=open(arch,O_RDWR|O_CREAT,S_IRWXU);
	if(fd1<0){
		printf("Error al compactar");
		return;
	}	
	for(int i=0;i<*conteo;i++){
		char *clave=keys[i];
		char *valor=get_val(clave, baseabierta);
		int dist=lseek(fd1,0,SEEK_END);
		if (dist<0){
			return;
		}
		char str[200];
		sprintf(str,"%s:%s\n",clave,valor);
		int n=rio_writen(fd1,str,strlen(str));
		if (n<0){
			printf("Error al compactar");
			return;
		}
		put(baseabierta->ht,clave,&dist); 
	}
	baseabierta->rutalog=arch;	
}

