#include <sys/types.h>          
#include <sys/stat.h>
#include <stdio.h>              
#include <stdlib.h>             
#include <stddef.h>             
#include <string.h>             
#include <unistd.h>             
#include <signal.h>              
#include <netdb.h> 
#include <errno.h> 
#include <syslog.h> 
#include <sys/socket.h> 
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/resource.h>
#include <ctype.h>
#include "db.h"

conexionlogdb *conectar_db(char *ip, int puerto){
	conexionlogdb *conect= malloc(sizeof(conexionlogdb *));
	memset(&conect, 0, sizeof(conect));
	conect->ip=ip;
	conect->puerto=puerto;
	conect->id_sesion=0;
	conect->nombredb="";

	struct sockaddr_in direccion_servidor;

	memset(&direccion_servidor, 0, sizeof(direccion_servidor));
	

	direccion_servidor.sin_family = AF_INET;		
	direccion_servidor.sin_port = htons(puerto);		
	direccion_servidor.sin_addr.s_addr = inet_addr(ip) ;
	
	int fd;
	int err = 0;
	
	if((fd = socket(((struct sockaddr *)&direccion_servidor)->sa_family, SOCK_STREAM, 100)) < 0)
		printf("Error al inicializar el socket");
		return NULL;
		
	if(bind(fd, (struct sockaddr *)&direccion_servidor, sizeof(direccion_servidor)) < 0)
		goto errout;
	
	if(listen(fd, 1000) < 0)
		goto errout;
	
	errout:
	err = errno;
	close(fd);
	errno = err;
	fd=-1;
	
	if (fd<0){
		printf("Error al conectar");
		return NULL;
	}
	
	conect->sockdf= fd;

	return conect;
}
