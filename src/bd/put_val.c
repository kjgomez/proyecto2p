#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "hashtable.h"
#include "csapp.h"
#include "db.h"

int put_val(char *clave,char * valor,bdabierta *baseabierta){
	int fd= baseabierta->fdlog;
	if (fd<0){
		return 0;
	}	
	int dist=lseek(fd,0,SEEK_END);
	if (dist<0){
		return 0;
	}
	char str[200];
	sprintf(str,"%s:%s\n",clave,valor);
	int n=rio_writen(fd,str,strlen(str));
	if (n<0){
		printf("No se pudo escribir en el archivo");
		return 0;
	}
	put(baseabierta->ht,clave,&dist);
	return 1;
}
