#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include "hashtable.h"

#define BUFLEN 128 
//Estructuras 
typedef struct basedatosabierta{
    char *rutalog;
    char *rutaindex;
    int  fdlog;
    hashtable *ht;
} bdabierta;



typedef struct conexionlogbTDA{
    char *ip;
    int puerto;              
    int sockdf;         //socket de la conexion a la base de datos.
    int id_sesion;		//Inicialmente en 0. Usado por el proceso logdb para saber que proceso esta usando cual db.
				//Al abrir la conexion, se llenar� este campo con un numero aleatorio (Este numero lo genera el proceso logdb, y lo devuelve a la lib).             
    char *nombredb;		//nombre de la db en uso. Inicialmente vacio hasta que se llame abrir_db(). Se llena SOLO si el proceso logdb pudo 
				//Abrir exitosamente los archivos de la DB (log e indice)
} conexionlogdb;

//Interfaz

conexionlogdb *conectar_db(char *ip, int puerto);			//Devuelve un objeto conexio, NULL si no se pudo abrir conexion.

int crear_db(char *nombre_db, char *directorio);			//Recibe un objeto conexion, y crear la db con el nombre dado. Devuelve 1 en exito, 0 en error (por ejemplo,
									//ya existe un base de datos con ese nombre, error de E/S, etc).

int abrir_db(conexionlogdb *conexion, char *nombre_db,char *directorio,bdabierta *baseabierta);			//Recibe un objeto conexion. Con esto se informa al proceso logdb que abra la base de datos nombre_db.
									//Retorna 1 si se pudo abrir, 0 en error (por ejemplo, si dicha db no existe).

int put_val(char *clave, char *valor, bdabierta *basebierta);		//Inserta el elemento clave:valor en la db. Devuelve 1 en exito, 0 en  error.

char *get_val(char *clave, bdabierta *baseabierta); 				//devuelve el valor asociado a clave (si existe). Devuelve NULL si la clave no existe.

int eliminar(char *clave,bdabierta *baseabierta);				//Elimina la clave. Devuelve 1 en exito, 0 en error.

void cerrar_db(conexionlogdb *conexion,bdabierta *baseabierta);				//Cierra la conexion y libera la estructura conexion.

void compactar(conexionlogdb *conexion,bdabierta *baseabierta);				//Permite forzar la compactacion de la db.

void reemplazarCarater(char *str, char viejo, char nuevo);
void llenarHash(hashtable *tabla, char *archivo);
char* directorio(char *nombre_db);




