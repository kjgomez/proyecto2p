#Escriba su makefiile en este archivo
#
# Pedro Mendoza	
# Kevin Gomez

#Si quieren llamar reglas adicionales, agregenlas como pre-requisitos a la regla all
#La regla logdb.so deben modificarla con su comando para generar dicho ejecutable.
#El archivo prueba.c esta vacio. NO deben llenarlo. Este se reemplazara con el codigo del profesor
#al probar su proyecto

all: logdb prueba

prueba: prueba.o liblogdb.so
	gcc obj/prueba.o lib/liblogdb.so -Llib/ -o bin/prueba

prueba.o: src/prueba.c
	gcc -Wall -Iinclude/ -c src/prueba.c -o obj/prueba.o


logdb: abrir_db_s.o cerrar_db_s.o compactar_s.o conexion_db_s.o crear_db_s.o csapp_s.o eliminar_s.o get_val_s.o put_val_s.o servidor.o reemplazarCaracter.o libhashtab.so
	gcc -L./lib obj/abrir_db_s.o obj/cerrar_db_s.o obj/compactar_s.o obj/csapp_s.o obj/conexion_db_s.o obj/crear_db_s.o obj/eliminar_s.o obj/get_val_s.o obj/reemplazarCaracter.o obj/put_val_s.o obj/servidor.o lib/libhashtab.so -o bin/logdb -pthread


crear_db_s.o: src/bd/crear_db.c
	gcc -Wall -c src/bd/crear_db.c -o obj/crear_db_s.o -I include/ -g

abrir_db_s.o: src/bd/abrir_db.c
	gcc -Wall -c src/bd/abrir_db.c -o obj/abrir_db_s.o -I include/ -g

compactar_s.o: src/bd/compactar.c
	gcc -Wall -c src/bd/compactar.c -o obj/compactar_s.o -I include/ -g

cerrar_db_s.o: src/bd/cerrar_db.c
	gcc -Wall -c src/bd/cerrar_db.c -o obj/cerrar_db_s.o -I include/ -g

conexion_db_s.o: src/bd/conexion_db.c
	gcc -Wall -c src/bd/conexion_db.c -o obj/conexion_db_s.o -I include/ -g

csapp_s.o: src/bd/csapp.c
	gcc -Wall -c src/bd/csapp.c -o obj/csapp_s.o -I include/ -g

eliminar_s.o: src/bd/eliminar.c
	gcc -Wall -c src/bd/eliminar.c -o obj/eliminar_s.o -I include/ -g

put_val_s.o: src/bd/put_val.c
	gcc -Wall -c src/bd/put_val.c -o obj/put_val_s.o -I include/ -g

get_val_s.o: src/bd/get_val.c
	gcc -Wall -c src/bd/get_val.c -o obj/get_val_s.o -I include/ -g

servidor.o: src/servidor.c
	gcc -Wall -c src/servidor.c -o obj/servidor.o -I include/ -g

reemplazarCaracter.o: src/bd/reemplazarCaracter.c
	gcc -Wall -c src/bd/reemplazarCaracter.c -o obj/reemplazarCaracter.o -I include/ -g






liblogdb.so: crear_db.o abrir_db.o cerrar_db.o compactar.o conexion_db.o csapp.o eliminar.o get_val.o put_val.o
	gcc -Wall -fPIC -shared obj/crear_db.o obj/abrir_db.o obj/cerrar_db.o obj/compactar.o obj/conexion_db.o obj/csapp.o obj/eliminar.o obj/get_val.o obj/put_val.o -o  lib/liblogdb.so

libhashtab.so: numeroElementos.o crearHashTable.o put.o get.o remover.o borrar.o claves.o contieneClave.o valores.o hash.o
	gcc -shared -fPIC -o lib/libhashtab.so obj/crearHashTable.o obj/numeroElementos.o obj/put.o obj/get.o obj/remover.o obj/borrar.o obj/claves.o obj/contieneClave.o obj/valores.o obj/hash.o

crearHashTable.o: src/hashtable/crearHashTable.c 
	gcc -Wall -fPIC -c src/hashtable/crearHashTable.c -o obj/crearHashTable.o -I include/ -g
numeroElementos.o: src/hashtable/borrar.c
	gcc -Wall -fPIC -c src/hashtable/numeroElementos.c -o obj/numeroElementos.o -I include/ -g
put.o: src/hashtable/put.c 
	gcc -Wall -fPIC -c src/hashtable/put.c -o obj/put.o -I include/ -g
get.o: src/hashtable/get.c
	gcc -Wall -fPIC -c src/hashtable/get.c -o obj/get.o -I include/ -g
remover.o: src/hashtable/remover.c
	gcc -Wall -fPIC -c src/hashtable/remover.c -o obj/remover.o -I include/ -g
borrar.o: src/hashtable/borrar.c
	gcc -Wall -fPIC -c src/hashtable/borrar.c -o obj/borrar.o -I include/ -g
claves.o: src/hashtable/claves.c
	gcc -Wall -fPIC -c src/hashtable/claves.c -o obj/claves.o -I include/ -g
contieneClave.o: src/hashtable/contieneClave.c 
	gcc -Wall -fPIC -c src/hashtable/contieneClave.c -o obj/contieneClave.o -I include/ -g
valores.o: src/hashtable/valores.c
	gcc -Wall -fPIC -c src/hashtable/valores.c -o obj/valores.o -I include/ -g
hash.o: src/hashtable/hash.c
	gcc -Wall -fPIC -c src/hashtable/hash.c -o obj/hash.o -I include/ -g

crear_db.o: src/logdb/crear_db.c
	gcc -Wall -fPIC -c src/logdb/crear_db.c -o obj/crear_db.o -I include/ -g

abrir_db.o: src/logdb/abrir_db.c
	gcc -Wall -fPIC -c src/logdb/abrir_db.c -o obj/abrir_db.o -I include/ -g

compactar.o: src/logdb/compactar.c
	gcc -Wall -fPIC -c src/logdb/compactar.c -o obj/compactar.o -I include/ -g

cerrar_db.o: src/logdb/cerrar_db.c
	gcc -Wall -fPIC -c src/logdb/cerrar_db.c -o obj/cerrar_db.o -I include/ -g

conexion_db.o: src/logdb/conexion_db.c
	gcc -Wall -fPIC -c src/logdb/conexion_db.c -o obj/conexion_db.o -I include/ -g

csapp.o: src/logdb/csapp.c
	gcc -Wall -fPIC -c src/logdb/csapp.c -o obj/csapp.o -I include/ -g

eliminar.o: src/logdb/eliminar.c
	gcc -Wall -fPIC -c src/logdb/eliminar.c -o obj/eliminar.o -I include/ -g

put_val.o: src/logdb/put_val.c
	gcc -Wall -fPIC -c src/logdb/put_val.c -o obj/put_val.o -I include/ -g

get_val.o: src/logdb/get_val.c
	gcc -Wall -fPIC -c src/logdb/get_val.c -o obj/get_val.o -I include/ -g


.PHONY: clean
clean:
	rm bin/* obj/*.o lib/*.so

